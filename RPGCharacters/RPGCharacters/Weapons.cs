﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGCharacters
{
    public class Weapon : Item
    {
        public WeaponTypes WeaponType { get; set; }
        public int BaseDamage { get; set; }
        public double AttacksPerSecond { get; set; }
    }
}
