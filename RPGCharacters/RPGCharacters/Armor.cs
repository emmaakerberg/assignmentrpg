﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGCharacters
{
    public class Armor : Item
    {
        public PrimaryAttribute primaryAttribute = new PrimaryAttribute();

        public ArmorTypes ArmorType { get; set; }

        /// <summary>
        /// Constructor that sets values to the primary attributes.
        /// </summary>
        public Armor ()
        {
            primaryAttribute.Vitality = 1;
            primaryAttribute.Strength = 1;
            primaryAttribute.Dexterity = 1;
            primaryAttribute.Intelligence = 1;
        }
        public ArmorTypes EquippedArmorType { get; set; }
    }
}
