﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGCharacters
{
    public class Item
    {
        public string Name { get; set; }
        public int RequiredLevel { get; set; }
        public SlotTypes Slot { get; set; }

    }
}
