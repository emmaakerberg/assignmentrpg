﻿using System;

namespace RPGCharacters
{
    public class Program
    {
        static void Main(string[] args)
        {
            //----------------------------------------------------------------
            //--------------------------- M A G E ----------------------------
            //----------------------------------------------------------------
            Mage mageObj = new Mage() { Name = "Harry", Level = 1 };
            mageObj.ShowStats();
            mageObj.LevelUp(1);
            mageObj.ShowStats();

            //tries to equipp a weapon. If an exception is thrown, a message is catched, that shows what went wrong.
            try
            {
                Weapon weaponObjMage = new Weapon() { Name = "Poff", RequiredLevel = 2, Slot = SlotTypes.Weapon, WeaponType = WeaponTypes.Wand, BaseDamage = 3, AttacksPerSecond = 2 };
                mageObj.EquipWeapon(weaponObjMage);
            }
            catch (InvalidWeaponException e)
            {
                Console.WriteLine(e.Message);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            mageObj.ShowStats();

            //tries to equipp an armor. If an exception is thrown, a message is catched, that shows what went wrong.
            try
            {
                Armor armorObjMage = new Armor() { Name = "Piff", RequiredLevel = 2, Slot = SlotTypes.Body, ArmorType = ArmorTypes.Cloth };
                mageObj.EquipArmor(armorObjMage);
            }
            catch (InvalidWeaponException e)
            {
                Console.WriteLine(e.Message);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            mageObj.ShowStats();

            //----------------------------------------------------------------
            //------------------------- R A N G E R --------------------------
            //----------------------------------------------------------------

            Ranger rangerObj = new Ranger() { Name = "Emma", Level = 1 };
            rangerObj.ShowStats();
            rangerObj.LevelUp(1);
            rangerObj.ShowStats();

            //tries to equipp a weapon. If an exception is thrown, a message is catched, that shows what went wrong.
            try
            {
                Weapon weaponObjRanger = new Weapon() { Name = "Poff", RequiredLevel = 2, Slot = SlotTypes.Weapon, WeaponType = WeaponTypes.Bow, BaseDamage = 3, AttacksPerSecond = 2 };
                rangerObj.EquipWeapon(weaponObjRanger);
            }
            catch (InvalidWeaponException e)
            {
                Console.WriteLine(e.Message);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            rangerObj.ShowStats();

            //tries to equipp an armor. If an exception is thrown, a message is catched, that shows what went wrong.
            try
            {
                Armor armorObjRanger = new Armor() { Name = "Piff", RequiredLevel = 2, Slot = SlotTypes.Legs, ArmorType = ArmorTypes.Leather };
                rangerObj.EquipArmor(armorObjRanger);
            }
            catch (InvalidWeaponException e)
            {
                Console.WriteLine(e.Message);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            rangerObj.ShowStats();

            //----------------------------------------------------------------
            //-------------------------- R O G U E ---------------------------
            //----------------------------------------------------------------

            Rogue rogueObj = new Rogue() { Name = "Ron", Level = 1 };
            rogueObj.ShowStats();
            rogueObj.LevelUp(1);
            rogueObj.ShowStats();

            //tries to equipp a weapon. If an exception is thrown, a message is catched, that shows what went wrong.
            try
            {
                Weapon weaponObjRogue = new Weapon() { Name = "Poff", RequiredLevel = 2, Slot = SlotTypes.Weapon, WeaponType = WeaponTypes.Dagger, BaseDamage = 3, AttacksPerSecond = 2 };
                rogueObj.EquipWeapon(weaponObjRogue);
            }
            catch (InvalidWeaponException e)
            {
                Console.WriteLine(e.Message);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            rogueObj.ShowStats();

            //tries to equipp an armor. If an exception is thrown, a message is catched, that shows what went wrong.
            try
            {
                Armor armorObjRogue = new Armor() { Name = "Piff", RequiredLevel = 2, Slot = SlotTypes.Legs, ArmorType = ArmorTypes.Mail };
                rogueObj.EquipArmor(armorObjRogue);
            }
            catch (InvalidWeaponException e)
            {
                Console.WriteLine(e.Message);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            rogueObj.ShowStats();

            //----------------------------------------------------------------
            //------------------------- W A R R I O R --------------------------
            //----------------------------------------------------------------

            Warrior warriorObj = new Warrior() { Name = "Nevil", Level = 1 };
            warriorObj.ShowStats();
            warriorObj.LevelUp(1);
            warriorObj.ShowStats();

            //tries to equipp a weapon. If an exception is thrown, a message is catched, that shows what went wrong.
            try
            {
                Weapon weaponObjWarrior = new Weapon() { Name = "Poff", RequiredLevel = 2, Slot = SlotTypes.Weapon, WeaponType = WeaponTypes.Hammer, BaseDamage = 3, AttacksPerSecond = 2 };
                warriorObj.EquipWeapon(weaponObjWarrior);
            }
            catch (InvalidWeaponException e)
            {
                Console.WriteLine(e.Message);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            warriorObj.ShowStats();

            //tries to equipp an armor. If an exception is thrown, a message is catched, that shows what went wrong.
            try
            {
                Armor armorObjWarrior = new Armor() { Name = "Piff", RequiredLevel = 2, Slot = SlotTypes.Body, ArmorType = ArmorTypes.Plate };
                warriorObj.EquipArmor(armorObjWarrior);
            }
            catch (InvalidWeaponException e)
            {
                Console.WriteLine(e.Message);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            warriorObj.ShowStats();
        }
    }
}
