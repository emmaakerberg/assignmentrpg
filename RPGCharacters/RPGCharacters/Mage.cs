﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGCharacters
{
    public class Mage : Character
    {
        public PrimaryAttribute primaryAttribute = new PrimaryAttribute();
        public SecondaryAttribute secondaryAttribute = new SecondaryAttribute();

        Weapon weaponObj;
        Armor armorObj;
        int totalSumAttributes;

        /// <summary>
        /// Constructor that sets values to the primary and secondary attributes.
        /// </summary>
        public Mage ()
        {
            primaryAttribute.Vitality = 5;
            primaryAttribute.Strength = 1;
            primaryAttribute.Dexterity = 1;
            primaryAttribute.Intelligence = 8;

            secondaryAttribute.Health = primaryAttribute.Vitality * 10;
            secondaryAttribute.ArmorRating = primaryAttribute.Strength + primaryAttribute.Dexterity;
            secondaryAttribute.ElementalResistance = primaryAttribute.Intelligence;
            secondaryAttribute.DPS = 1;
            Level = 1;
        }

        /// <summary>
        ///    Levels up the character and increases the values for the primary and secondary attributes.
        /// </summary>
        /// <param name="level">The number of levels the character increases</param>
        public void LevelUp(int level)
        {
            if(level <= 0)
            {
                throw new ArgumentException();
            }

            primaryAttribute.Vitality += level * 3;
            primaryAttribute.Strength += level * 1;
            primaryAttribute.Dexterity += level * 1;
            primaryAttribute.Intelligence += level * 5;

            secondaryAttribute.Health = primaryAttribute.Vitality * 10;
            secondaryAttribute.ArmorRating = primaryAttribute.Strength + primaryAttribute.Dexterity;
            secondaryAttribute.ElementalResistance = primaryAttribute.Intelligence;

            Level += level;
        }

        /// <summary>
        /// Checks if the weapon that the character tries to equipp has the correct weapon type and required level.
        /// A function is called to add the weapon if it is correct.
        /// If it is the wrong weapon type or the required level is to high, an exception is thrown.
        /// </summary>
        /// <param name="weapon">The weapon object that the character wants to equipp.</param>
        /// <returns>Message telling that the weapon is equipped.</returns>
        /// <exception cref="InvalidWeaponException">If the weapon is the wrong weapon type or the weapons required level is to high.</exception>
        public string EquipWeapon (Weapon weapon)
        {
            string message = "New weapon equipped";

            //If characters level is the same as or higher that the required level for the weapon
            if (weapon.RequiredLevel <= Level)
            {
                //If the weapon type is Staff or Wand 
                if (weapon.WeaponType == WeaponTypes.Staff || weapon.WeaponType == WeaponTypes.Wand)
                {
                    //If it is, a function is called that adds the weapon to the character
                    weaponObj = weapon;
                    AddWeaponToCharacter(weaponObj);
                    return message;
                } 
                else
                {
                    // If we get here, we know that the weapon type was wrong
                    throw new InvalidWeaponException("Wrong Weapon");
                }
            } 
            else
            {
                // If we get here, we know that the required level of the weapon is to high
                throw new InvalidWeaponException("Wrong Level");
            }
        }

        /// <summary>
        /// Checks if the armor that the character tries to equipp has the correct armor type and required level.
        /// A function is called to add the armor to a dictionary if it is correct.
        /// If it is the wrong armor type or the required level is to high, an exception is thrown.
        /// </summary>
        /// <param name="armor">The armor object that the character wants to equipp.</param>
        /// <returns>Message telling that the armor is equipped.</returns>
        /// <exception cref="InvalidArmorException">If the armor is the wrong armor type or the armors required level is to high.</exception>
        public string EquipArmor(Armor armor)
        {
            string message = "New armor equipped";

            //If characters level is the same as or higher that the required level for the armor
            if (armor.RequiredLevel <= Level)
            {
                // If the armor type is Cloth
                if (armor.ArmorType == ArmorTypes.Cloth)
                {
                    //If it is, 3 functions are called. 1 = adds the weapon to the character. 2 = calculates the value of the primary atributes. 3 = calculates the characters DPS;
                    armorObj = armor;
                    AddArmorToDictionary(armorObj, armor.Slot);
                    totalSumAttributes = CalcTotalAttributes(armorObj);
                    CalcDPS();
                    return message;
                }
                else
                {
                    // If we get here, we know that the armor type was wrong
                    throw new InvalidArmorException("Wrong Armor");
                }
            }
            else
            {
                // If we get here, we know that the required level of the armor is to high
                throw new InvalidArmorException("Wrong Level");
            }
        }

        /// <summary>
        /// Adds the equipped armor into the armor dictionary.
        /// </summary>
        /// <param name="armor">the armor object that the character is equipping</param>
        /// <param name="slotType">which slot the armor should be placed in</param>
        public void AddArmorToDictionary(Armor armor, SlotTypes slotType)
        {
            ArmorSlots.Add(slotType, armor);
        }

        /// <summary>
        /// Adds the equpped weapon to a variable.
        /// </summary>
        /// <param name="weapon">the weapon object that the character is equipping</param>
        public void AddWeaponToCharacter(Weapon weapon)
        {
            Weapon = weapon;
        }

        /// <summary>
        /// Calculates the value of the primary attributes from the character and the armor.
        /// </summary>
        /// <param name="armorObj">the armor object that is equipped</param>
        /// <returns>The total value of the primary attributes from the character and the armor</returns>
        public int CalcTotalAttributes(Armor armorObj)
        {
            int SumCharacterAttributes = primaryAttribute.Intelligence;
            int SumArmorAttributes = armorObj.primaryAttribute.Intelligence;

            int totalSumAttributes = SumArmorAttributes + SumCharacterAttributes;

            return totalSumAttributes;
        }

        /// <summary>
        /// Calculates the characters DPS.
        /// </summary>
        public void CalcDPS ()
        {
            // If a weapon object exists and is not null
            if (weaponObj != null)
            {
                // If it is, the base damage and attacks per second for the weapon is included in the calculation of the characters DPS
                secondaryAttribute.DPS = (weaponObj.BaseDamage * weaponObj.AttacksPerSecond) * (1 + totalSumAttributes / 100);
            } 
            else
            {
                // If it isn't, the weapon DPS in the calculation is set to 1.
                secondaryAttribute.DPS = 1 * (1 + totalSumAttributes / 100);
            }
        }

        /// <summary>
        /// Displays the stats of the character into the console.
        /// </summary>
        public void ShowStats()
        {
            Console.WriteLine("-------- Show Stats for the Mage --------");
            Console.WriteLine($"Character name: {Name}");
            Console.WriteLine($"Character level: {Level}");
            Console.WriteLine($"Strength: {primaryAttribute.Strength}");
            Console.WriteLine($"Dexterity: {primaryAttribute.Dexterity}");
            Console.WriteLine($"Intelligence: {primaryAttribute.Intelligence}");
            Console.WriteLine($"Health: {secondaryAttribute.Health}");
            Console.WriteLine($"Armor Rating: {secondaryAttribute.ArmorRating}");
            Console.WriteLine($"Elemental Resistance: {secondaryAttribute.ElementalResistance}");
            Console.WriteLine($"DPS: {secondaryAttribute.DPS}");
            Console.WriteLine("-----------------------------------------");
        }
    }
}
