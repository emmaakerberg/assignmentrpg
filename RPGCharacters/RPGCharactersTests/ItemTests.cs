﻿using System;
using Xunit;
using RPGCharacters;

namespace RPGCharactersTests
{
    public class ItemTests
    {
        [Fact]
        public void EquipWeapon_WhenRequiredLevelIsTooHigh_ShouldThrowException()
        {
            //Arrange
            Mage mageObj = new Mage();
            Weapon weaponObj = new Weapon() { Name = "Poff", RequiredLevel = 2, Slot = SlotTypes.Head, WeaponType = WeaponTypes.Wand, BaseDamage = 3, AttacksPerSecond = 2 };

            //Act & Assert
            Assert.Throws<InvalidWeaponException>(() => mageObj.EquipWeapon(weaponObj));
        }

        [Fact]
        public void EquipArmor_WhenRequiredLevelIsTooHigh_ShouldThrowException()
        {
            //Arrange
            Mage mageObj = new Mage();
            Armor armorObj = new Armor() { Name = "Piff", RequiredLevel = 3, Slot = SlotTypes.Legs, ArmorType = ArmorTypes.Cloth };

            //Act & Assert
            Assert.Throws<InvalidArmorException>(() => mageObj.EquipArmor(armorObj));
        }

        [Fact]
        public void EquipWeapon_WhenWeaponTypeIsWrong_ShouldThrowException()
        {
            //Arrange
            Mage mageObj = new Mage();
            Weapon weaponObj = new Weapon() { Name = "Poff", RequiredLevel = 1, Slot = SlotTypes.Head, WeaponType = WeaponTypes.Axe, BaseDamage = 3, AttacksPerSecond = 2 };

            //Act & Assert
            Assert.Throws<InvalidWeaponException>(() => mageObj.EquipWeapon(weaponObj));
        }

        [Fact]
        public void EquipArmor_WhenArmorTypeIsWrong_ShouldThrowException()
        {
            //Arrange
            Mage mageObj = new Mage();
            Armor armorObj = new Armor() { Name = "Piff", RequiredLevel = 1, Slot = SlotTypes.Legs, ArmorType = ArmorTypes.Leather };

            //Act & Assert
            Assert.Throws<InvalidArmorException>(() => mageObj.EquipArmor(armorObj));
        }

        [Fact]
        public void EquipWeapon_WhenWeaponIsCorrect_ShouldShowWeaponIsEquipped()
        {
            //Arrange
            Mage mageObj = new Mage();
            Weapon weaponObj = new Weapon() { Name = "Poff", RequiredLevel = 1, Slot = SlotTypes.Weapon, WeaponType = WeaponTypes.Wand, BaseDamage = 3, AttacksPerSecond = 2 };
            //Act
            string expected = "New weapon equipped";
            //Assert
            Assert.Equal(expected, mageObj.EquipWeapon(weaponObj));
        }

        [Fact]
        public void EquipArmor_WhenArmorIsCorrect_ShouldShowArmorIsEquipped()
        {
            //Arrange
            Mage mageObj = new Mage();
            Armor armorObj = new Armor() { Name = "Piff", RequiredLevel = 1, Slot = SlotTypes.Legs, ArmorType = ArmorTypes.Cloth };
            //Act
            string expected = "New armor equipped";
            //Assert
            Assert.Equal(expected, mageObj.EquipArmor(armorObj));
        }

        [Fact]
        public void CalculateDPS_WhenNoWeaponIsEquipped_ShouldBeCorrectValue()
        {
            //Arrange
            Mage mageObj = new Mage();
            mageObj.CalcDPS();
            double expected = 1 * (1 + (8 / 100));
            //Act
            double actual = mageObj.secondaryAttribute.DPS;
            //Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void CalculateDPS_WhenWeaponIsEquipped_ShouldBeCorrectValue()
        {
            //Arrange
            Mage mageObj = new Mage();
            Weapon weaponObj = new Weapon() { Name = "Poff", RequiredLevel = 1, Slot = SlotTypes.Head, WeaponType = WeaponTypes.Wand, BaseDamage = 7, AttacksPerSecond = 1.1 };
            mageObj.EquipWeapon(weaponObj);
            mageObj.CalcDPS();
            double expected = (7 * 1.1) * (1 + (5 / 100));
            //Act
            double actual = mageObj.secondaryAttribute.DPS;
            //Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void CalculateDPS_WhenWeaponAndArmorIsEquipped_ShouldBeCorrectValue()
        {
            //Arrange
            Mage mageObj = new Mage();
            Weapon weaponObj = new Weapon() { Name = "Poff", RequiredLevel = 1, Slot = SlotTypes.Head, WeaponType = WeaponTypes.Wand, BaseDamage = 7, AttacksPerSecond = 1.1 };
            mageObj.EquipWeapon(weaponObj);
            Armor armorObj = new Armor() { Name = "Piff", RequiredLevel = 1, Slot = SlotTypes.Legs, ArmorType = ArmorTypes.Cloth };
            mageObj.EquipArmor(armorObj);
            mageObj.CalcDPS();
            double expected = (7 * 1.1) * (1 + (5 / 100));
            //Act
            double actual = mageObj.secondaryAttribute.DPS;
            //Assert
            Assert.Equal(expected, actual);
        }
    }
}
