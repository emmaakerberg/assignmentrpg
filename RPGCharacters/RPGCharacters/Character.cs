﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGCharacters
{
    public class Character
    {
        public string Name { get; set; }
        public int Level { get; set; }

        public Dictionary<SlotTypes, Armor> ArmorSlots { get; set; }
        public Weapon Weapon { get; set; }

        /// <summary>
        /// Constructor that create a dictionary for the armor
        /// </summary>
        public Character()
        {
            ArmorSlots = new Dictionary<SlotTypes, Armor>();
        }

    } 
}
