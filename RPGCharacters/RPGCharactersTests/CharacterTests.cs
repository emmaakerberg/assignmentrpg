using System;
using Xunit;
using RPGCharacters;

namespace RPGCharactersTests
{
    public class CharacterTests
    {
        [Fact]
        public void Level_WhenCharacterIsCreated_ShouldBeLevelOne()
        {
            // Arrange
            Mage mageObj = new Mage();
            int expected = 1;
            // Act
            int actual = mageObj.Level;
            // Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Level_WhenLevelingUpOnce_ShouldBeLevelTwo()
        {
            //Arrange
            Mage mageObj = new Mage();
            mageObj.LevelUp(1);
            int expected = 2;
            //Act
            int actual = mageObj.Level;
            //Assert
            Assert.Equal(expected, actual);
        }

        [Theory]
        [InlineData(0)]
        [InlineData(-1)]
        public void Level_WhenTryingToLevelUpNegativeValue_ShouldThrowException(int InlineData)
        {
            //Arrange
            Mage mageObj = new Mage();

            //Act & Assert
            Assert.Throws<ArgumentException>(() => mageObj.LevelUp(InlineData));
        }

        [Fact]
        public void Attributes_WhenMageIsCreated_ShouldBeDefaultAttributes()
        {
            //Arrange
            Mage mageObj = new Mage();
            int expected = 5;
            //Act
            int actual = mageObj.primaryAttribute.Vitality;
            //Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Attributes_WhenRangerIsCreated_ShouldBeDefaultAttributes()
        {
            //Arrange
            Ranger rangerObj = new Ranger();
            int expected = 7;
            //Act
            int actual = rangerObj.primaryAttribute.Dexterity;
            //Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Attributes_WhenRogueIsCreated_ShouldBeDefaultAttributes()
        {
            //Arrange
            Rogue rogueObj = new Rogue();
            int expected = 6;
            //Act
            int actual = rogueObj.primaryAttribute.Dexterity;
            //Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Attributes_WhenWarriorIsCreated_ShouldBeDefaultAttributes()
        {
            //Arrange
            Warrior warriorObj = new Warrior();
            int expected = 5;
            //Act
            int actual = warriorObj.primaryAttribute.Strength;
            //Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Attributes_WhenMageLevelsUp_ShouldBeIncreased()
        {
            //Arrange
            Mage mageObj = new Mage();
            mageObj.LevelUp(1);
            int expected = 5 + 3;

            //Act
            int actual = mageObj.primaryAttribute.Vitality;

            //Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Attributes_WhenRangerLevelsUp_ShouldBeIncreased()
        {
            //Arrange
            Ranger rangerObj = new Ranger();
            rangerObj.LevelUp(1);
            int expected = 7 + 5;

            //Act
            int actual = rangerObj.primaryAttribute.Dexterity;

            //Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Attributes_WhenRogueLevelsUp_ShouldBeIncreased()
        {
            //Arrange
            Rogue rogueObj = new Rogue();
            rogueObj.LevelUp(1);
            int expected = 6 + 4;

            //Act
            int actual = rogueObj.primaryAttribute.Dexterity;

            //Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Attributes_WhenWarriorLevelsUp_ShouldBeIncreased()
        {
            //Arrange
            Warrior warriorObj = new Warrior();
            warriorObj.LevelUp(1);
            int expected = 5 + 3;

            //Act
            int actual = warriorObj.primaryAttribute.Strength;

            //Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Attributes_WhenCharacterLevelsUp_HealthShouldBeIncreased()
        {
            //Arrange
            Mage mageObj = new Mage();
            mageObj.LevelUp(1);
            int expected = 80;

            //Act
            int actual = mageObj.secondaryAttribute.Health;

            //Assert
            Assert.Equal(expected, actual);
        }
    }
}
